# Ruminate MonoGame GUI #

Current Version: MonoGame Beta
This project is a GUI library for the MonoGame library.

By: [Scott Franks](http://scott-franks.com/)

### Demonstration Project: ###
There is a BitBucket repo [here](https://bitbucket.org/ClassicThunder/ruminate-gui-example) that contains an example project demonstrating how to set up and use the GUI. Each of the files in the screen folder demonstrate different capabilities and you can toggle though them when running the example project by pressing tab. 

### Skinning System: ###
Easily alter the look of the widgets to fit the theme of your project.
Each widget can be individually skinned allowing you to mix and match skins.

### Currently Available Widgets: ###
* Single Line Textbox
* Plain Text Textbox
* Button
* Toggle Button
* Check box
* Combo Box
* Labels
* Images
* Panel
* Scroll Panel
* Slider	


### Input Management: ###
As of release 1.4.0 the library uses a tweaked version of [starbound-input](https://bitbucket.org/rbwhitaker/starbound-input). I have only done minimal testing so am calling this release a beta since a lot of testing needs to occur before I'm confident in the new input system.